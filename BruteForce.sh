#!/bin/bash
# GNU bash, version 4.4.20
ALPHABET=('a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z')

PASSWORD=(0 0 0 0 0 0 0 0 0 0 0)
PASSWORD_SIZE=11

determineIsLetter() {
  if [[ $1 -lt 3 ]]; then
    echo 0
  elif [[ $1 -lt 5 ]]; then
    echo 1
  elif [[ $1 -lt 8 ]]; then
    echo 0
  else
    echo 1
  fi
}

determineSymbol() {
  local value=$1
  local isLetter=$2
  
  if [[ isLetter -eq 0 ]]; then
    echo ${ALPHABET[$value]}
  else
    echo $value
  fi
}

determineMaxValue() {
  if [[ $1 -lt 3 ]]; then
    echo 25
  elif [[ $1 -lt 5 ]]; then
    echo 9
  elif [[ $1 -lt 8 ]]; then
    echo 25
  else
    echo 9
  fi
}

update() {
  for ((i = $PASSWORD_SIZE-1 ; i >= 0; i--)); do
    local maxValue=$(determineMaxValue $i)
    if [[ $maxValue -eq ${PASSWORD[$i]} ]]; then
      PASSWORD[i]=0
    else
      PASSWORD[i]="$((${PASSWORD[$i]} + 1))"
      break
    fi
  done
}

buildPassword() {
  word=$PASSWORD
  for ((i = 0 ; i < $PASSWORD_SIZE ; i++)); do
    local isLetter=$(determineIsLetter $i)
    local symbol=$(determineSymbol ${PASSWORD[$i]} $isLetter)    
    word[i]=$symbol
  done
  echo "${word[@]}"
}

enterPassword() {
  #try entering maybePassword to website
  return 1
}

doAThing() {
  while true; do
    local maybePassword=$(buildPassword)
    local didItWork=$(enterPassword $maybePassword)
    
    if [[ didItWork -eq 0 ]]; then
      echo "IT WORKED!!! $maybePassword"
      break
    else
      update
    fi
    count="$(($count + 1))"
    echo $count
  done
}

doAThing